<?php

use App\Http\Controllers\CRUDController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->group(function (){
    Route::get('/users', [CRUDController::class, 'readUsers']);
    Route::get('/users/{user}', [CRUDController::class, 'read']);
    Route::post('/create-users', [CRUDController::class, 'create']);
    Route::put('/users/{user}', [CRUDController::class, 'update']);
    Route::delete('/users/{user}', [CRUDController::class, 'delete']);
});
