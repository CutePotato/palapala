<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserCreateQuantityRequest;
use App\Http\Requests\UserReadRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Models\User;
use App\Services\Facades\RandomUser;
use Illuminate\Support\Facades\Hash;

class CRUDController extends Controller
{
    public function readUsers(UserReadRequest $request)
    {
        return User::all();
    }

    public function read(User $user, UserReadRequest $request)
    {
        return $user;
    }

    public function create(UserCreateQuantityRequest $request)
    {
        $validData = $request->validated();
        $quantity = $validData['quantity'];
        return RandomUser::createRandomUsers($quantity);
    }

    public function update(User $user, UserUpdateRequest $request){
        $validData = $request->validated();
        if (isset($validData['email'])) $user->email = $validData['email'];
        if (isset($validData['password'])) $user->password = Hash::make($validData['password']);
        if (isset($validData['name'])) $user->name = $validData['name'];
        $user->save();
        return $user;
    }

    public function delete(User $user){
        $user->delete();
    }
}
