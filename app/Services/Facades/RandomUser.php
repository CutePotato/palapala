<?php

namespace App\Services\Facades;

use Illuminate\Support\Facades\Facade;

class RandomUser extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'randomUser';
    }
}
