<?php

namespace App\Services;

use App\Events\UserCreated;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;

class RandomUser
{
    const RANDOM_USER_API = 'https://randomuser.me/api/';

    public function getRandomUser(int $quantity = 1){
        return Http::get(self::RANDOM_USER_API, [
            'results' => $quantity
        ])->json();
    }

    public function createRandomUsers(int $quantity = 1){
        $data = $this->getRandomUser($quantity);
        $users = collect();
        foreach ($data['results'] as $result){
            $user = User::create([
                'name' => $result['login']['username'],
                'email' => $result['email'],
                'password' => Hash::make($result['login']['password'])
            ]);
            UserCreated::dispatch($user);
            $users->add($user);
        }

        return $users;
    }

}
