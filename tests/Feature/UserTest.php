<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UserTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_dashboard()
    {
        $user = User::factory()->create();

        $response = $this->actingAs($user)->get('/dashboard');

        $response->assertStatus(200);
    }

    public function test_read_users(){
        $user = User::factory()->create();

        $response = $this->actingAs($user)->get('/api/users');

        $response->assertOk();
    }

    public function test_read_user(){
        $user = User::factory()->create();

        $response = $this->actingAs($user)->get('/api/users/'.$user->id);

        $response->assertOk();
    }

    public function test_create_user(){
        $user = User::factory()->create();

        $response = $this->actingAs($user)->post('/api/create-users', [
            'quantity' => 4
        ]);

        $response->assertOk();
    }

    public function test_update_user(){
        $user = User::factory()->create();

        $this->actingAs($user)->put('/api/users/'.$user->id, [
            'email' => 'hola@hola.es',
            'name' => 'Pepe',
            'password' => ''
        ]);

        $this->assertDatabaseHas('users', [
            'email' => 'hola@hola.es'
        ]);
    }

    public function test_delete_user(){
        $user = User::factory()->create();

        $this->actingAs($user)->delete('/api/users/'.$user->id);

        $this->assertDeleted($user);
    }
}
