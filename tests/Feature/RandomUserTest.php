<?php

namespace Tests\Feature;

use App\Services\Facades\RandomUser;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class RandomUserTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_get_random_user()
    {
        $response = RandomUser::getRandomUser(1);
        $this->assertArrayHasKey('results', $response);
    }

    public function test_create_random_user(){
        $users = RandomUser::createRandomUsers(3);
        foreach ($users as $user){
            $this->assertDatabaseHas('users', ['id' => $user->id]);
        }
    }
}
